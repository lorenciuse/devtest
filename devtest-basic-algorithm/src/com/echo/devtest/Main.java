package com.echo.devtest;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n <= 0 || n > 100) return;
        int blankCol = n - 1;
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (col < blankCol) {
                    System.out.print("  ");
                } else if (col == blankCol) {
                    System.out.print("#");
                } else {
                    System.out.print(" #");
                }
            }
            --blankCol;
            System.out.println();
        }
    }
}