# README

There are two folders :
- **devtest-basic-algorithm**
The first one is a solution for Test 1
- **devtest-rest-api**
The second one is a solution for Test 2

# HOW TO

To run the solution of Test 1, you can try executing it with the following command without the build process.
```sh
java -jar devtest-basic-algorithm.jar
```

And to run the solution of Test 2, you need to change the database credential to match yours, and then create its database. In this case MySql database is used.

**spring.datasource.username**
**spring.datasource.password**

```sql
create database customer_management
```

```sh
cd devtest-rest-api
mvn spring-boot:run
```

After the application started up, you can try to run the postman collection.
