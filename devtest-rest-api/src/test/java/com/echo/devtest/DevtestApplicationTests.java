package com.echo.devtest;

import com.echo.devtest.payload.request.CreateCustomerRequest;
import com.echo.devtest.payload.request.LoginRequest;
import com.echo.devtest.payload.request.SignupRequest;
import com.echo.devtest.payload.request.UpdateCustomerRequest;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DevtestApplicationTests {

	@Autowired
	private TestRestTemplate template;

	@Value("${lorencho.app.jwtCookieName}")
	private String jwtCookie;

	@Test
	@Order(1)
	void givenAuthRegisterRequest_whenUserAlreadyExists_shouldFailedWith400() {
		SignupRequest request = new SignupRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		request.setEmail("user2@devtest.com");
		request.setRole(new HashSet<>(List.of("ROLE_USER")));
		ResponseEntity<String> result = template.postForEntity("/api/auth/signup", request, String.class);

		Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("Error: Username is already taken!"));
	}

	@Test
	@Order(2)
	void givenAuthLoginRequest_whenUserAlreadyExists_shouldSucceedWith200() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> result = template.postForEntity("/api/auth/signin", request, String.class);

		Assertions.assertEquals(HttpStatus.OK.value(), result.getStatusCode().value());
		System.out.println(result.getHeaders().get(HttpHeaders.SET_COOKIE));
		Assertions.assertTrue(Objects.requireNonNull(result.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).contains(jwtCookie));
	}

	@Test
	@Order(3)
	void givenAuthenticatedUser_whenAccessGetAllCustomerAndCustomerNotExists_shouldFailedWith404() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		ResponseEntity<String> result = template.exchange("/api/customers", HttpMethod.GET, new HttpEntity<>(headers), String.class);

		Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("No customer data found"));
	}

	@Test
	@Order(4)
	void givenAuthenticatedUser_whenAccessGetSpecificCustomerAndCustomerNotExists_shouldFailedWith404() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		ResponseEntity<String> result = template.exchange("/api/customers/1", HttpMethod.GET, new HttpEntity<>(headers), String.class);

		Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("Customer not found"));
	}

	@Test
	@Order(5)
	void givenAuthenticatedUser_whenAccessCreate1stCustomerAndCustomerNotExists_shouldSucceedWith201() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		CreateCustomerRequest requestCustomer = new CreateCustomerRequest();
		requestCustomer.setName("Customer 1");
		requestCustomer.setPhone("+628111111");
		requestCustomer.setEmail("cust1@devtest.com");
		requestCustomer.setAddress("Jakarta");

		ResponseEntity<String> result = template.exchange("/api/customers", HttpMethod.POST, new HttpEntity<>(requestCustomer, headers), String.class);

		Assertions.assertEquals(HttpStatus.CREATED.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("Customer data created successfully"));
	}

	@Test
	@Order(6)
	void givenAuthenticatedUser_whenAccessCreate2ndCustomerAndCustomerNotExists_shouldSucceedWith201() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		CreateCustomerRequest requestCustomer = new CreateCustomerRequest();
		requestCustomer.setName("Customer 2");
		requestCustomer.setPhone("+628222222");
		requestCustomer.setEmail("cust2@devtest.com");
		requestCustomer.setAddress("Depok");

		ResponseEntity<String> result = template.exchange("/api/customers", HttpMethod.POST, new HttpEntity<>(requestCustomer, headers), String.class);

		Assertions.assertEquals(HttpStatus.CREATED.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("Customer data created successfully"));
	}

	@Test
	@Order(7)
	void givenAuthenticatedUser_whenAccessGetAllCustomerAndCustomerExists_shouldSucceedWith302() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		ResponseEntity<String> result = template.exchange("/api/customers", HttpMethod.GET, new HttpEntity<>(headers), String.class);

		Assertions.assertEquals(HttpStatus.FOUND.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("id"));
	}

	@Test
	@Order(8)
	void givenAuthenticatedUser_whenAccessGetSpecificCustomerAndCustomerExists_shouldSucceedWith302() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		ResponseEntity<String> result = template.exchange("/api/customers/1", HttpMethod.GET, new HttpEntity<>(headers), String.class);

		Assertions.assertEquals(HttpStatus.FOUND.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("id"));
	}

	@Test
	@Order(9)
	void givenAuthenticatedUser_whenAccessUpdateSpecificCustomerAndCustomerNotExists_shouldFailedWith404() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		UpdateCustomerRequest requestCustomer = new UpdateCustomerRequest();
		requestCustomer.setName("Customer 3");
		requestCustomer.setPhone("+628333333");
		requestCustomer.setEmail("cust3@devtest.com");
		requestCustomer.setAddress("Jakarta");

		ResponseEntity<String> result = template.exchange("/api/customers/3", HttpMethod.PUT, new HttpEntity<>(requestCustomer, headers), String.class);

		Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("Customer not found therefore no data is updated"));
	}

	@Test
	@Order(10)
	void givenAuthenticatedUser_whenAccessUpdateSpecificCustomerAndCustomerExists_shouldSucceedWith200() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		UpdateCustomerRequest requestCustomer = new UpdateCustomerRequest();
		requestCustomer.setName("Customer 2");
		requestCustomer.setPhone("+628222222");
		requestCustomer.setEmail("cust2@devtest.com");
		requestCustomer.setAddress("Jakarta");

		ResponseEntity<String> result = template.exchange("/api/customers/2", HttpMethod.PUT, new HttpEntity<>(requestCustomer, headers), String.class);

		Assertions.assertEquals(HttpStatus.OK.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("Customer data updated successfully"));
	}

	@Test
	@Order(11)
	void givenAuthenticatedOtherUser_whenAccessGetSpecificCustomerAndCustomerExists_shouldFailedWith403() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user2");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		ResponseEntity<String> result = template.exchange("/api/customers/2", HttpMethod.GET, new HttpEntity<>(headers), String.class);

		Assertions.assertEquals(HttpStatus.FORBIDDEN.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("Cannot access other user's customer data!"));
	}

	@Test
	@Order(12)
	void givenAuthenticatedOtherUser_whenAccessUpdateSpecificCustomerAndCustomerExists_shouldFailedWith403() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user2");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		UpdateCustomerRequest requestCustomer = new UpdateCustomerRequest();
		requestCustomer.setName("Customer 2");
		requestCustomer.setPhone("+628222222");
		requestCustomer.setEmail("cust2@devtest.com");
		requestCustomer.setAddress("Depok");

		ResponseEntity<String> result = template.exchange("/api/customers/2", HttpMethod.PUT, new HttpEntity<>(requestCustomer, headers), String.class);

		Assertions.assertEquals(HttpStatus.FORBIDDEN.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("Cannot update other user's customer data!"));
	}

	@Test
	@Order(13)
	void givenAuthenticatedOtherUser_whenAccessDeleteSpecificCustomerAndCustomerExists_shouldFailedWith403() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user2");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		ResponseEntity<String> result = template.exchange("/api/customers/2", HttpMethod.DELETE, new HttpEntity<>(headers), String.class);

		Assertions.assertEquals(HttpStatus.FORBIDDEN.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("Cannot delete other user's customer data!"));
	}

	@Test
	@Order(14)
	void givenAuthenticatedUser_whenAccessDeleteSpecificCustomerAndCustomerExists_shouldSucceedWith200() {
		LoginRequest request = new LoginRequest();
		request.setUsername("user1");
		request.setPassword("P@ssw0rd");
		ResponseEntity<String> loginResult = template.postForEntity("/api/auth/signin", request, String.class);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", Objects.requireNonNull(loginResult.getHeaders().get(HttpHeaders.SET_COOKIE)).get(0).split(";")[0]);

		ResponseEntity<String> result = template.exchange("/api/customers/2", HttpMethod.DELETE, new HttpEntity<>(headers), String.class);

		Assertions.assertEquals(HttpStatus.OK.value(), result.getStatusCode().value());
		Assertions.assertTrue(Objects.requireNonNull(result.getBody()).contains("Customer data deleted successfully"));
	}
}
