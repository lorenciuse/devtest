INSERT INTO customer_management.roles(id,name) VALUES(1,'ROLE_ADMIN');
INSERT INTO customer_management.roles(id,name) VALUES(2,'ROLE_USER');

INSERT INTO customer_management.users(id,email,password,username) VALUES (1,'admin@devtest.com','$2a$10$QHfa7LBa54KKaN3xhv/uAO6B0qfNq2Dp.d/N4HkNlwDa82yWsLRga','admin');
INSERT INTO customer_management.users(id,email,password,username) VALUES (2,'user1@devtest.com','$2a$10$9Ba01FhOIVk43YEtcjkPDuOB4ZjwAVdyjCuGog4W1I9j79dv0.5x6','user1');
INSERT INTO customer_management.users(id,email,password,username) VALUES (3,'user2@devtest.com','$2a$10$zmbpDb9A8/zHbqfqVDcxiuS/tvlp7M/BQGOJLrAYtjXj5zr.2xDAC','user2');

INSERT INTO customer_management.user_roles(user_id,role_id) VALUES (1,1);
INSERT INTO customer_management.user_roles(user_id,role_id) VALUES (2,2);
INSERT INTO customer_management.user_roles(user_id,role_id) VALUES (3,2);