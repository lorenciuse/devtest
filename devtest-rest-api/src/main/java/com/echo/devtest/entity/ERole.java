package com.echo.devtest.entity;

public enum ERole {
    ROLE_ADMIN,
    ROLE_USER
}
