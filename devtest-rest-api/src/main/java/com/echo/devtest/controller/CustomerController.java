package com.echo.devtest.controller;

import com.echo.devtest.entity.Customer;
import com.echo.devtest.entity.ERole;
import com.echo.devtest.entity.User;
import com.echo.devtest.payload.request.CreateCustomerRequest;
import com.echo.devtest.payload.request.UpdateCustomerRequest;
import com.echo.devtest.payload.response.MessageResponse;
import com.echo.devtest.repository.CustomerRepository;
import com.echo.devtest.repository.UserRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<?> findAll(Authentication authentication) {
        List<Customer> listCust;
        if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(ERole.ROLE_ADMIN.name()))) {
            listCust = (List<Customer>) customerRepository.findAll();
            if (listCust.isEmpty()) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("No customer data found"));
            return new ResponseEntity<>(listCust, HttpStatus.FOUND);
        }
        Optional<User> user = userRepository.findByUsername(authentication.getName());
        listCust = customerRepository.findAllByUser(user.get());
        if (listCust.isEmpty()) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("No customer data found"));
        return new ResponseEntity<>(listCust, HttpStatus.FOUND);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<?> findById(@PathVariable Long id, Authentication authentication) {
        Optional<Customer> c = customerRepository.findById(id);
        if (c.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Customer not found"));
        }
        Customer customer = c.get();
        if(!Objects.equals(customer.getUser().getUsername(), authentication.getName())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new MessageResponse("Cannot access other user's customer data!"));
        }
        return new ResponseEntity<>(customer, HttpStatus.FOUND);
    }

    @PostMapping("")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<?> create(@Valid @RequestBody CreateCustomerRequest createCustomerRequest, Authentication authentication) {
        Optional<User> user = userRepository.findByUsername(authentication.getName());
        customerRepository.save(new Customer(createCustomerRequest.getName(), createCustomerRequest.getPhone(), createCustomerRequest.getEmail(), createCustomerRequest.getAddress(), user.get()));
        return ResponseEntity.status(HttpStatus.CREATED).body(new MessageResponse("Customer data created successfully"));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<?> update(@PathVariable Long id, @Valid @RequestBody UpdateCustomerRequest updateCustomerRequest, Authentication authentication) {
        Optional<Customer> c = customerRepository.findById(id);
        if (c.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Customer not found therefore no data is updated"));
        }
        Customer customer = c.get();
        if(!Objects.equals(customer.getUser().getUsername(), authentication.getName())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new MessageResponse("Cannot update other user's customer data!"));
        }
        customer.setName(updateCustomerRequest.getName());
        customer.setPhone(updateCustomerRequest.getPhone());
        customer.setEmail(updateCustomerRequest.getEmail());
        customer.setAddress(updateCustomerRequest.getAddress());
        customerRepository.save(customer);
        return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse("Customer data updated successfully"));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<?> delete(@PathVariable Long id, Authentication authentication) {
        Optional<Customer> c = customerRepository.findById(id);
        if (c.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Customer not found therefore no data is deleted"));
        }
        Customer customer = c.get();
        if(!Objects.equals(customer.getUser().getUsername(), authentication.getName())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new MessageResponse("Cannot delete other user's customer data!"));
        }
        customerRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse("Customer data deleted successfully"));
    }
}