package com.echo.devtest.repository;

import com.echo.devtest.entity.ERole;
import com.echo.devtest.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role,Long> {

    Optional<Role> findByName(ERole name);
}
