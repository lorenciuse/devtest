package com.echo.devtest.repository;

import com.echo.devtest.entity.Customer;
import com.echo.devtest.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends CrudRepository<Customer,Long> {

    List<Customer> findAllByUser(User id);
}
